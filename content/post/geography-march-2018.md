---
title: "Geography Update March 2018"
date: 2018-03-14T10:37:42+00:00
draft: false
---
The map is an important aspect of failmap. Without it, presentation and incentive to change wouldn't be as strong.

With mapping comes the challenge to make sure the map is correct. In the Netherlands we visualize municipalities, which
change every year: mainly merging and dissolving.

Getting accurate data is impossible to do by hand. Luckily there are many mapping guru's out there who have done the
heavy lifting and given it back to the world for all of us to enjoy.

In an upcoming release of failmap, we've for the first time automated A) the merging of municipalities and B) updating
the geography of the map. This post shows how we did it, and the results.

Merging and dissolving
----------------------
In 2018 a number of municipalities merged and one dissolved. Based on the wikipedia entry [for upcoming mergers in the Netherlands](https://nl.wikipedia.org/wiki/Gemeentelijke_herindelingen_in_Nederland#Komende_herindelingen)
simple code was written to handle these situations. It's hardly battle tested and it's not the ideal situation, but it
helped us through the first map changes we had to deal with.

The code looks like this:
```

"""
De gemeenten Menaldumadeel, Franekeradeel en Het Bildt zullen opgaan in een nieuwe gemeente Waadhoeke.
"""

# We used the frysian name: Menameradiel instead of Menaldumadeel
merge(source_organizations_names=["Menameradiel", "Franekeradeel", "Het Bildt"],
      target_organization_name="Waadhoeke",
      when=merge_date,
      organization_type="municipality",
      country="NL"
      )

"""
Ook de dorpen Welsrijp, Winsum, Baijum en Spannum van gemeente Littenseradiel,
sluiten zich bij deze nieuwe gemeente aan.

Littenseradiel verdwijnt dus. De nieuwe gemeenten mogen de erfenis opruimen.
"""
dissolve(dissolved_organization_name="Littenseradiel",
     target_organization_names=["Waadhoeke", "Leeuwarden", "Súdwest-Fryslân"],
     when=merge_date,
     organization_type="municipality",
     country="NL"
     )
```

The code has various shortcomings, yet it's fine for the moment and will be addressed at more frequent mergers.

Tech details: For example it is unable to merge different types of organizations. We've also found
out that the many to many relation between organization and url can benefit from "from when" and "until when" columns:
when dissolving now, a clone of the target organizations is made.


Using Open Streetmap for mapping data
-------------------------------------

[Openstreetmap](https://openstreetmap.org) is awesome. It has many API's (such as overpass) that help with extracting
data. While nothing with mapping is truly easy, it does it's job if you invest some time in it.

Thanks to [overpass](https://overpass-api.de) we where able to automatically extract a high resolution map of
municipalities in the Netherlands. We did so using this query:

```
area[name="Nederland"]->.gem;
relation(area.gem)["type"="boundary"][admin_level=8];
out geom;
```

The output can be converted to geojson using the [osmtogeojson](https://github.com/tyrasd/osmtogeojson) tool.

Unfortunately the map data is about 50 megabyte, which is far too much for normal visitors to download every time. So
the map data needs to be reduced in some way. Luckily this has already been done a dozen of time and the magic word
is Ramer-Douglas-Peucker algorithm from the 70's, which is implemented by talented programmers in nearly every language.
Failmap uses the [python version by Fabian Hirschmann](https://github.com/fhirschmann/rdp).


Stacking history
----------------

Failmap wants to be accurate over time. For this the data model was changed to store both organizations and coordinate
history over time. Using the [stacking pattern](https://failmap.readthedocs.io/en/latest/topics/stacking_pattern.html)
we can make a fast database query that will give the correct geography, organizations and ratings over time.

The meat of the query is:

```sql
INNER JOIN
(SELECT id as stacked_organization_id
      FROM organization stacked_organization
      WHERE (stacked_organization.created_on <= '%s' AND stacked_organization.is_dead == 0)
      OR (
      '%s' BETWEEN stacked_organization.created_on AND stacked_organization.is_dead_since
      AND stacked_organization.is_dead == 1)) as organization_stack
      ON organization_stack.stacked_organization_id = map_organizationrating.organization_id
```

Using these types of queries, we get an answer in milliseconds. It was the fastest in our benchmark and it returns the
same results on both sqlite and mysql. (We've seen queries that give different results on different databases due to
differences in optimization and implementation).

The end result
--------------
On Januari 2018 the mergers happened, and we've implemented them way too late (March 2018). So we're slightly altering
history by adding the mergers on the first of January 2018. We'll soon add the new domains for the newly created
organizations.

What you can see:

- Higher resolution shapes
- Mergers (and dissolving) in the left (Waadhoeke, Leeuwarden), and a merger on the right (Westerwolde).

Enjoy!

Week 52 of 2017:
![failmap.org](/geography-march-2018/week_52_2017.png)

Week 1 of 2018:
![failmap.org](/geography-march-2018/week_1_2018.png)
