---
title: "Update Februari 2018"
date: 2018-02-12T10:10:51+01:00
draft: false
---
Failmap has been the driving force behind fixing thousands of vulnerabilities!
This makes us very proud and we're just getting started.

Thanks for reading this blog. So much has happened, so there is a lot to share. \0/

This is a community project. All of our work is public, including all code and data. [You are welcome to join!](mailto:info@faalkaart.nl)


Status update - Sept 2017 to Feb 2018
-------------------------------------

Successes:

- failmap has already been the driving force behind fixing thousands of vulnerabilities(!)
- we've run hundreds of thousands of unobtrusive scans, thousands added daily,
- we've discovered thousand of domains, that are also scanned


New features:

- Extensive reporting
- Full screen view mode
- Graphs showing vulnerabilities over time
- Insights into new scan results
- Faster historical navigation


Technological successes:

- With a [over a thousand commits](https://gitlab.com/failmap/failmap/commits/master), nearly everything changed!
- easier development, deployment and hosting (vagrant, tox, docker)
- preparing for international release (importing maps, creating translations)
- development fully on Gitlab, see https://gitlab.com/failmap/
- integration of OpenStreetMap
- migrated all scans to asynchronous and distributed
- automated code quality validation
- uptime, no matter what happens in the backend, we are always available
- using a more sane and stable technology stack
- fast caching, the site stays fast, regardless how many urls and organizations
- tons of refactors, bugfixes and improvements
- cleaned the project a few times, as less code is better :)


Challenges:

- scaling up: continuation of the project for years to come
- we've received hundreds of questions and remarks that need manual updating: we need people that process this
- the software needs to be even easier to develop and install, making it possible for journalists and other novices
- distributed task schedulers are in an early stage and require several advancements before it can handle millions of things


Failures:

- Of course we have bugs: sometimes the map was gray or not updated continuously.
- We've asked for donations in the wrong way: even with extremely spammy buttons and easy payment we got 8 euro out of a few thousand visits.
- Not having stickers/banners to promote the organization (they are ordered now).


The future
----------

As we're nearing a stable product, the project will slightly refocus from development to continuation:
what's the value of an awesome project if nobody is using it and it slowly dies? That would be a total waste!

Continuation means things like promoting, meetings and other factors that increase social stability around the
project. This year we hope to find the community that will continue supporting this project (and it's fruits) indefinitely.

Of course there will be a series of technological improvements to make this project even more relevant.

This is a short list of conference we'll attend (as a village, speaker etc):

- Hacker Hotel, failmap village, 16-18 february (hackerhotel.nl)
- Hack Talk, speaking (#6, April)
- Electromagnetic Field, failmap village, 31 August - September 2nd

A lot more, to be determined - Send a request to info@faalkaart.nl
