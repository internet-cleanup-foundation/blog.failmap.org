Failmap blog
============

Uses [Git](https://git-scm.com/) to store content.
Rendered using [Hugo](https://gohugo.io/), [Installation manual](https://gohugo.io/getting-started/installing)


Writing content
---------------
In your shell:

```
git clone https://gitlab.com/failmap/blog.failmap.org/
cd blog.failmap.org
hugo server -D
```

In your browser:
http://localhost:1313/

All edits are semi-instant. If you don't know where to start, copy-paste an existing article.
Articles are written in Markdown.


Saving articles
---------------
Store your changes in git. Preferably do so in a pull request if you've not written a post for failmap yet.

Once there are multiple people writing on the same post, we might change below instructions :)

```git
git stash
git pull --rebase origin master
git stash apply
```

Resolve possible merge conflicts now.

```git
git add -A
git commit -m "describe your changes"
git push
```

Ask the blog admin to post the new version.


Publishing the blog
-------------------

https://gohugo.io/getting-started/usage/
